// Smooth scroll To
var tagN = 0;
var tag1array = ["Aladdin,", "Sashimi,", "Oreo,", "Unicorn,", "Roller,", "Batman,"]
var tag2array = ["Genie", "Wasabi", "Milk", "Rainbow", "Coaster", "Robin"]

$(window).bind("load", function() {
  $('body').addClass('ready');

  setTimeout(function(){
    $('.wc-spinner-large').fadeOut('fast');
  }, 1000);
});

$(document).ready(function() {
  // Scrollspy initiation
  $('body').scrollspy({ 
    target: '.navbar',
    offset: 60
  });

  // Add smooth scrolling on all links inside the navbar
  $("#navbar a").on('click', function(event) {
    // Make sure this.hash has a value before overriding default behavior
    if (this.hash !== "") {
      // Prevent default anchor click behavior
      event.preventDefault();

      // Store hash
      var hash = this.hash;

      // Using jQuery's animate() method to add smooth page scroll
      // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
      $('html, body').animate({
        scrollTop: $(hash).offset().top
      }, 800, function(){
   
        // Add hash (#) to URL when done scrolling (default click behavior)
        window.location.hash = hash;
      });
    }  // End if
  });

  // Add sticky header experience
  $(window).scroll(function () {
    if ($(window).scrollTop() > 30) {
      $('body').addClass('navbar-fixed');
    }
    if ($(window).scrollTop() <= 30) {
      $('body').removeClass('navbar-fixed');
    }
  });

  setInterval(function(){
    $('#tag1, #tag2, #tag3, #tag4').animate({'opacity': '0'}, 200, function(){
      $('#tag2').html(tag1array[tagN]);
      $('#tag4').html(tag2array[tagN]);
      $('#tag1, #tag2').animate({'opacity': '1'}, 400, function(){
        $('#tag3, #tag4').animate({'opacity': '1'}, 400);
      });
    });
    tagN++;
    if (tagN == 5) { tagN = 0; }
  }, 6000);
});

// Set up tooltips
$('a[data-toggle="tooltip"]').tooltip();

// Set up dropdown
$('.dropdown-toggle').dropdown();
