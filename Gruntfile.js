module.exports = function(grunt) {

    // Project configuration.
    grunt.initConfig({
 
        //Read the package.json (optional)
        pkg: grunt.file.readJSON('package.json'),
 
        // Metadata.
        meta: {
            basePath: '',
            srcPath: 'js/',
            deployPath: 'deploy/'
        },
 
        
        // ==================================
        // TASKS CONFIG


        // Clean Up Previous Deployment - 
        // https://github.com/gruntjs/grunt-contrib-clean
        clean: {
            deploy: ["deploy"],
        },


        // COPY FILES FROM SOURCE TO DEPLOY
        // https://github.com/gruntjs/grunt-contrib-copy
        copy: {
            awesome: {
              files: [{
                expand: true,
                cwd: "bower_components/font-awesome/fonts",
                src: ["**"],
                dest: "deploy/fonts/",
              }]
            },
            dev: {
              files: [{
                expand: true,
                cwd: "app",
                src: ["img/**","css/**"],
                dest: "deploy/",
              }]
            },
            deploy: {
              files: [{
                expand: true,
                cwd: "app",
                src: ["css/**"],
                dest: "deploy/",
              },{
                expand: true,
                src: ["Web.Config"],
                dest: "deploy/",
              }]
            }
        },


        // LESS - https://github.com/gruntjs/grunt-contrib-less
        less: {
          dev: {
            options: {
              sourceMap: true,
              sourceMapFilename: 'deploy/css/xpt.min.css.map',
              sourceMapRootpath: '',
              sourceMapBasepath: 'deploy/css/',
            },
            files: [
                { src: ["app/less/index.less"], dest: "deploy/css/xpt.min.css"}
            ],
          },
          deploy: {
            options: {
              sourceMap: true ,
              sourceMapFilename: 'deploy/css/xpt.min.css.map',
              sourceMapRootpath: '',
              sourceMapBasepath: 'deploy/css/',

              syncImport: true,
              compress: true,
              cleancss: true, 
              optimization: 2
            },
            files: [
                { src: ["app/less/index.less"], dest: "deploy/css/xpt.min.css"}
            ],
          }
        },


        // split CSS for IE selector limit - https://github.com/Ponginae/grunt-bless
        bless: {
            dev: {
                options: {
                    compress: false,
                    cleanup: true,
                    imports: false
                },
                files: [ 
                    { src: ["deploy/css/xpt.min.css"], dest: "deploy/css/xpt.min.ie.css"},
                ]
            },
            deploy: {
                options: {
                    compress: true,
                    cleanup: true,
                    imports: false
                },
                files: [ 
                    { src: ["deploy/css/xpt.min.css"], dest: "deploy/css/xpt.min.ie.css"},
                ]
            }
        },


        // UGLIFY
        // https://github.com/gruntjs/grunt-contrib-uglify
        uglify: {
            dev: {
              options: {
                mangle: false,
                compress: false,
                beautify: true
              },
              files: [{ 
                  src: [
                      //Bootstrap
                      'bower_components/bootstrap/js/transition.js',
                      'bower_components/bootstrap/js/alert.js',
                      'bower_components/bootstrap/js/button.js',
                      'bower_components/bootstrap/js/carousel.js',
                      'bower_components/bootstrap/js/collapse.js',
                      'bower_components/bootstrap/js/dropdown.js',
                      'bower_components/bootstrap/js/modal.js',
                      'bower_components/bootstrap/js/tooltip.js',
                      'bower_components/bootstrap/js/popover.js',
                      'bower_components/bootstrap/js/scrollspy.js',
                      'bower_components/bootstrap/js/tab.js',
                      'bower_components/bootstrap/js/affix.js', 

                      //Our Scripts
                      'app/js/script.js', 
                  ],
                  dest: "deploy/js/libraries.min.js"
              },{ 
                  src: [
                      'bower_components/particles.js/particles.min.js',
                      'app/js/homepage.js'
                  ],
                  dest: "deploy/js/particles.min.js"
              },{ 
                  src: [
                      'bower_components/respond/src/respond.js',
                      'bower_components/html5shiv/dist/html5shiv.js'
                  ],
                  dest: "deploy/js/ie9.min.js"
              }],
            },
            deploy: {
              options: {
                report: 'min'
              },
              files: "<%= uglify.dev.files %>",
            },
        },


        // Pug Templates
        // https://github.com/gruntjs/grunt-contrib-pug
        pug: {
          dev: {
            options : {
              pretty: true,
            },
            files: [{
                expand: true,
                cwd: "app/pug/pages",
                src: ["**/*.pug"],
                dest: "deploy/",
                ext: ".html"
            }],
          },
          deploy: {
            options : {
              pretty: false
            },
            files: "<%= pug.dev.files %>",
          }
        },
        

        // CONNECT - Create server - https://github.com/gruntjs/grunt-contrib-connect
        connect: {
            server: {
              options: {
                port: 9999,
                base: 'deploy'
              }
            }
        },


        // WATCH FILES & FOLDERS - https://github.com/gruntjs/grunt-contrib-watch
        // Executes the listed targets on file save
        // Watches folders for file changes and then runs the specified tasks
        watch: {
            options: {
              interrupt: true,
              files: ['!deploy/**','!bower_components/**','!node_modules/**']
            },
            uglify: {
                files: ["app/js/**"],
                tasks: ['uglify:dev'],
            },
            less: {
                files: ['app/less/**'],
                tasks: ['less:dev'],
            },
            pug: {
                files: ['app/pug/**'],
                tasks: ['pug:dev'],
            },
            copy: {
                files: ["app/css/**"],
                tasks: ['copy:dev'],
            }
        },

	// Copy to live folder
	shell: {
            webdeploy: {
		command: [
			'rsync -r -u -v deploy/ /var/www/xpt.io/web/',
			'chown -R xptio:xptio /var/www/xpt.io/web/',
		].join('&&')
        	}
    	},
    });
 
    // These plugins provide necessary tasks.
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-imagemin');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-pug');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-connect');
    grunt.loadNpmTasks('grunt-bless');
    grunt.loadNpmTasks('grunt-shell');


    // Test Deployment
    grunt.registerTask('testDeploy', ['clean:deploy', 
                                      'copy:awesome',
                                      'copy:deploy',
                                      'imagemin:deploy',
                                      'less:deploy',
                                      'bless:deploy',
                                      'uglify:deploy',
                                      'pug:deploy',
                                      'connect:server:keepalive']);

    // Default Run
    grunt.registerTask('default', ['clean:deploy',
                                   'copy:awesome',
                                   'copy:dev',
                                   'less:dev',
                                   'bless:dev',
                                   'uglify:dev',
                                   'pug:dev',
                                   'connect',
				   'shell:webdeploy',
                                   'watch']);
};
